from random import randrange
from bitstring import BitArray


class HCLib:
    @staticmethod
    def generate_secret():
        return randrange(500, 10000) * 2 + 1

    @staticmethod
    def encrypt_char(m, secret_key):
        bin_value = bin(m)
        encrypt_mass = []
        k = 1000
        array = list(bin_value[2:])
        for i in range(len(array), 32):
            array.insert(0, 0)

        for i in array:
            r = randrange(0, 100)
            digit = int(i)
            z = 2 * r + digit
            # encrypt number
            c = z + secret_key * randrange(0, 100)
            encrypt_mass.append(c)
        return encrypt_mass

    @staticmethod
    def encrypt(input_string, secret_key):
        result_array = []
        input_string=input_string.zfill(32)
        for i in input_string:
            result_array.append(HCLib.encrypt_char(ord(i), secret_key))
        return result_array

    @staticmethod
    def decrypt_char(encrypt_array, secret_key):
        decrypt_mass = []
        for i in encrypt_array:
            decrypt_mass.append((i % secret_key) % 2)
        return BitArray(decrypt_mass).uint

    @staticmethod
    def decrypt(encrypt_array, secret_key):
        result_str = ''
        for i in encrypt_array:
            result_str += chr(HCLib.decrypt_char(i, secret_key))
        return result_str.lstrip("0")

    @staticmethod
    def check_if_encrypted_string_are_equals(encrypted_data_first, encrypted_data_second, secret):
        result_array = []
        for i in range(0, len(encrypted_data_first)):
            char_array = []
            for j in range(0, len(encrypted_data_first[i])):
                char_array.append(abs(encrypted_data_first[i][j] + encrypted_data_second[i][j]))
            result_array.append(char_array)

        return HCLib.verifying(result_array, secret)

    @staticmethod
    def verifying(result_array, secret):
        for i in result_array:
            if HCLib.decrypt_char(i, secret) != 0:
                return False
        return True