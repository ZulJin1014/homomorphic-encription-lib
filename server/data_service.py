import ast


class Service:
    def __init__(self):
        pass

    def insert_value_in_file(self, data):
        with open("data.txt", "a") as file:
            file.write(str(data)+"\n")
            file.close()

    def find_values(self, data):
        self.validate_data(data)

        with open("data.txt", "r") as file:
            db_data = file.readlines()
            result_array = []
            for line in db_data:
                matrix = ast.literal_eval(line)

                line_result_matrix = []
                for i in range(0, 32):
                    word_array = []
                    for j in range(0, 32):
                        word_array.append(data[i][j] + matrix[i][j])
                    line_result_matrix.append(word_array)
                result_array.append(line_result_matrix)
            file.close()
        return result_array

    def validate_data(self, data):
        if len(data) != 32:
            raise Exception("Wrong encrypt data")
        for i in data:
            if len(i) != 32:
                raise Exception("wrong encrypt data")
