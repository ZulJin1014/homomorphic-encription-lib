import ast

import flask
from flask import Flask
from flask import request

from data_service import Service

app = Flask(__name__)
data_service = Service()


@app.route('/insert', methods=['POST'])
def hello_world():
    data = request.get_json(silent=True, force=True)
    data_service.insert_value_in_file(data)


@app.route('/find', methods=['POST'])
def find():
    data = request.get_json(silent=True, force=True)
    response_data = data_service.find_values(data)

    return flask.jsonify(response_data)

if __name__ == '__main__':
    app.run()
